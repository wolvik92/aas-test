function cleanup(e) {
  $.destroy();
}

function openPerson(e) {
  Ti.App.Properties.setString('personId', e.itemId);
  const personWindow = Alloy.createController('person').getView();
  $.navWindow.openWindow(personWindow);
}

function openPhoneApp(e) {
  Ti.Platform.openURL('tel:+421902177262');
}

function openMailApp(e) {
  Ti.Platform.openURL('mailto:vwolf135@gmail.com');
}

const people = Alloy.Collections.person;
const xhr = Ti.Network.createHTTPClient({
  onload: function(e) {
    const data = JSON.parse(this.responseText);
    const models = data.map(item => Alloy.createModel('person', item));
    people.reset(models);
  },
  onerror: function(e) {
    alert('Nastala chyba pri sťahovaní dát.');
  },
  timeout: 5000
});
xhr.open('GET', 'https://63da6fbb2af48a60a7cdc739.mockapi.io/api/v1/sample_data');
xhr.send();

$.version.text = `${Ti.App.version}.0102202301`;

$.navWindow.open();
