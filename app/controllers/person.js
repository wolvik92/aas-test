const people = Alloy.Collections.person;
const id = Ti.App.Properties.getString('personId');
const person = people.get(id);

const rowData = [
  { label: 'ID', value: person.get('id') },
  { label: 'Meno', value: person.get('name') },
  { label: 'E-mail', value: person.get('email') },
  { label: 'Telefón', value: person.get('phone_number') },
  { label: 'Obľúbená farba', value: person.get('favourite_color') },
  { label: 'Vytvorené', value: person.get('createdAt') }
];

const rows = rowData.map(item => {
  const row = Ti.UI.createTableViewRow({
    height: 40,
    layout: 'horizontal'
  });
  const label = Ti.UI.createLabel({
    text: `${item.label}: `,
    left: 16,
    center: {y: '50%'},
    font: {
      fontWeight: 'bold'
    }
  });
  const val = Ti.UI.createLabel({
    text: item.value,
    center: {y: '50%'}
  })
  row.add(label);
  row.add(val);
  return row;
});

$.image.image = person.get('avatar');
$.table.data = rows;
